import axios from 'axios'; 
 
import { getToken } from './local-storage-actions'; 
 
const instance = axios.create({ 
  baseURL: 'http://localhost:8000', 
  headers: { 'Content-Type': 'application/json' }, 
}); 
 
instance.interceptors.request.use(function (config) { 
  const token = getToken(); 
 
  if (config.headers.Authorization) { 
    return config; 
  } 
 
  return { 
    ...config, 
    headers: { 
      ...config.headers, 
      Authorization: `Bearer ${token}` 
    } 
  }; 
}, function (error) { 
  return Promise.reject(error); 
}); 
 
instance.interceptors.response.use(function (response) { 
  return response; 
}, function (error) { 
  return Promise.reject(error); 
}); 
 
 
export const AxiosInstance = ({ url, method = 'GET', params, data, headers }) => instance({ url, method, params, data, headers });
