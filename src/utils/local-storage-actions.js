const TOKEN = 'token'; 
const MOVIES = 'movies';
 
export const getToken = () => { 
  return window.localStorage.getItem(TOKEN);
} 
 
export const setToLocalStorage = (token) => { 
  if(token !== undefined){
    window.localStorage.setItem(TOKEN, token) 
  }
}

export const logOut = () => { 
  localStorage.clear(TOKEN);
  window.location.href = '/auth';
} 

export const removeFavorites = () => {
  localStorage.removeItem(MOVIES);
}
