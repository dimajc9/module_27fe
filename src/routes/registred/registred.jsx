import { useEffect } from 'react'; 
import hookForm from 'hook-easy-form'; 
import { useHistory } from 'react-router-dom'; 
import { useDispatch, useSelector } from 'react-redux'; 
 
import { runRegister } from '../../store/auth/actions'; 
import Input from '../../component/input/text-input'; 
  
import './registred.scss'; 
 
const form = [  
  { 
    name: 'email', 
    value: '', 
    required: true, 
    options: { 
      type: 'email', 
      label: 'Your email' 
    } 
  }, 
  { 
    name: 'password', 
    value: '', 
    required: true, 
    options: { 
      type: 'password', 
      label: 'Your password' 
    }, 
    validate: { 
      maxLength: (v) => (v.trim().length < 6 ? 'Invalid' : ''), 
    }, 
  }, 
] 
 
const Registred = () => { 
  const dispatch = useDispatch(); 
   const history = useHistory(); 
 
   const { formArray, updateEvent, valid, disabled, submitEvent } = hookForm({ initialForm: form }) 
   const registeredUser = useSelector((s) => s.auth.registeredUser); 

   useEffect(() => {
     if (registeredUser) history.push('/auth');
   }, [registeredUser, history])
     
   const submit = submitEvent((v) => { 
      dispatch(runRegister(v)) 
   }); 
 
   return ( 
    <div className='registr'>
      <div> 
        <form className='form' onSubmit={submit}> 
          {formArray.map(el => ( 
            <div key={el.name}> 
              <Input 
                value={el.value} 
                name={el.name} 
                label={el.options.label} 
                type={el.options.type} 
                onChange={updateEvent} /> 
            </div> 
          ))} 
          <button type="submit" disabled={disabled || !valid}>Submit</button> 
          <a>If you are registred?</a>
          <button onClick={() => history.push('/auth')}>Go to autharization</button>
        </form> 
      </div> 
    </div>
  )
} 
 
export default Registred;
