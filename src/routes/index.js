import { lazy } from 'react'; 
 
export const ROUTES = [ 
  { 
    id: 0, 
    exact: true, 
    private: true, 
    path: '/', 
    component: lazy(() => import('./main/main')), 
  }, 
  { 
    id: 1, 
    exact: true, 
    private: false, 
    path: '/auth', 
    component: lazy(() => import('./auth/auth')), 
  }, 
  { 
    id: 2, 
    exact: true, 
    private: false, 
    path: '/registred', 
    component: lazy(() => import('./registred/registred')), 
  },
  { 
    id: 3, 
    exact: true, 
    private: true, 
    path: '/favorite', 
    component: lazy(() => import('./basket/basket')), 
  },
]