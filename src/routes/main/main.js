import React, { useState, useEffect } from 'react';
import Pagination from '@material-ui/lab/Pagination';
import axios from 'axios';

import Header from '../../component/header/header'

import './main.scss'

const img_url = 'https://image.tmdb.org/t/p/w1280';
const api_url = 'https://api.themoviedb.org/3/';
const api_key =  '04c35731a5ee918f014970082a0088b1';

const Main = () => {
  const [ items, setItems ] = useState([]);
  const [ page, setPage ] = useState(1);
  const [ totalPages, setTotalPages ] = useState(null);
  const [ isLoading, setIsLoading ] = useState(true);
  const [ currentItem, setCurrentItem ] = useState(null);
  const [ search, setSearch ] = useState('');
  const [ inputValue, setInputValue ] = useState('');
  const [ year, setYear ] = useState({ })
  const [ sortBy, setSortBy ] = useState('popularity.desc')

  useEffect(() => {
    setIsLoading(true);
      axios.get(`${api_url}${search ? 'search' : 'discover'}/movie?&api_key=${api_key}&primary_release_year=${year.year}&query=${search}&sort_by=${sortBy}&page=${page}&`)      
      .then(res => {
        const movies = res.data.results;
        setTotalPages(res.data.total_pages);
        setIsLoading(false);
        setPage(res.data.page);
        setItems(movies);
      }) 
  }, [ search, page, year, sortBy ])
  
  const buildItemInfo = (item) => {
    axios.get(`${api_url}movie/${item.id}?api_key=${api_key}`)
    .then(res => {
      setCurrentItem(res.data)
    })
  }
  
  const yearRender = () => {
    let arg = []
    for (var i = 1925; i <= 2022; i++) {
      arg[i] = +i
    }
    return arg
  }
  
  const sortByYear = () => {
    return yearRender().map((item, i) => {
        return (
          <option key={i} onClick={() => {setSearch(inputValue); setPage(1)}} value={item} >{item}</option>
        )
    }).reverse()
  }

  const buildItems = () => {
    return items.filter(item => item.poster_path).map((item, i) => { 
      return <div key={i} className={'films_wrapper'}>
        <div className={`films`} onClick={() => buildItemInfo(item)}>
          <img src={img_url + item.poster_path} alt={item.title}/>
          <div><p>{ item.title }</p></div>
        </div>
      </div>
    })
  }

  const numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  const paginationChange = (e, value) => {
    if( page !== value) {
      setItems([]);
      setPage(value);
    }
  }

  const throwToFavorites = () => {
    let itemToStore = {
      img_path: currentItem.poster_path,
      title: currentItem.title,
      overview: currentItem.overview
    }
    let currentMovies = JSON.parse(localStorage.getItem("movies") || "[]")
    if(!(currentMovies.filter(item => item.title === itemToStore.title).length > 0)){
      currentMovies.push(itemToStore);
      localStorage.setItem("movies", JSON.stringify(currentMovies));
    } 
  }

  const buildCurrentItem = () => {
    return <div className={'current_item'}>
      <div className='current_item_bakground' onClick={() => {setCurrentItem(null)}}> 
        <img src={img_url + currentItem.backdrop_path} />
      </div>
      <div className='current_item_content'>
        <div className='current_item_add_mylist'>
          <img onClick={() => setCurrentItem(null)} src='https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Ic_arrow_back_36px.svg/240px-Ic_arrow_back_36px.svg.png' />
          <button onClick={throwToFavorites}>Add to MyList</button>
        </div>
        <h1>{currentItem.title}</h1>
        <p>{currentItem.overview}</p>
        <div className='current_item_release'>
          <span className='span'>Release Date:  </span>
          <span> {currentItem.release_date}</span>
        </div>
        <div className='current_item_vote'>
          <span className='span'>Vote Average:  </span>
          <span> {currentItem.vote_average}</span>
        </div>
        <div className='current_item_budget'>
          <span className='span'>Budget:  </span>
          <span> ${numberWithCommas(currentItem.budget)}</span>
        </div>
        <div className='current_item_homepage'>
          <a href={currentItem.homepage}>Check Homepage</a>
          <div className='current_item_genres'>
            { currentItem.genres[0] ? <span> { currentItem.genres[0].name }</span> : "" }
            { currentItem.genres[1] ? <span> { currentItem.genres[1].name }</span> : "" }
            { currentItem.genres[2] ? <span> { currentItem.genres[2].name }</span> : "" }
          </div>
        </div>
      </div>
    </div>
  }

  return (
    <>
      { currentItem ? buildCurrentItem() : '' }
      { currentItem ?  '' : <Header />}
      { currentItem ?  '' : <div className='content'>
        <div className='select_form'>
          <div className='buttons_form'>
            <button onClick={() => setSortBy('popularity.asc')}>Popularity asc</button>
            <button onClick={() => setSortBy('popularity.desc')}>Popularity desc</button>
            <button onClick={() => setSortBy('release_date.asc')}>Release asc</button>
            <button onClick={() => setSortBy('release_date.desc')}>Release desc</button>
            <button onClick={() => setSortBy('revenue.asc')}>Revenue asc</button>
            <button onClick={() => setSortBy('revenue.desc')}>Revenue desc</button>
            <button onClick={() => setSortBy('vote_count.asc')}>Vote count asc</button>
            <button onClick={() => setSortBy('vote_count.desc')}>Vote count desc</button>
          </div>
            <div className='render-form'>              
              <select className='select_year' size='1' name="fruit" onChange={(e) => {setYear({ year: e.target.value })}} > 
                <option disabled>Выберите год</option>
                <option value=''>all year</option>
                {sortByYear()}
              </select>
            <form className='form' onSubmit={(e) => {
              e.preventDefault()
              setPage(1)
              setSearch(inputValue)
            }}>
              <input value={inputValue} onChange={(e) => setInputValue(e.target.value)}/>
              <button type="submit">Search</button>
            </form>
          </div>
        </div>
        <div className={'items-list'}>
          {isLoading && <img src='https://c.tenor.com/XK37GfbV0g8AAAAi/loading-cargando.gif'/>}
          {!isLoading && buildItems()}
        </div>
          {!isLoading && !currentItem && <div className='pagination'><Pagination count={totalPages} variant="outlined" color="primary" shape="rounded" onChange={paginationChange}/></div> }
      </div>}
    </>
  )
}

export default Main
