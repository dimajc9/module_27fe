import { useEffect } from 'react'; 
import hookForm from 'hook-easy-form'; 
import { useHistory } from 'react-router-dom'; 
import { useDispatch, useSelector } from 'react-redux'; 
 
import { runAuth } from '../../store/auth/actions'; 
import Input from '../../component/input/text-input'; 
 
import './auth.scss'; 
 
const form = [ 
  { 
    name: 'email', 
    value: '', 
    required: true, 
    options: { 
      type: 'email', 
      label: 'Your email' 
    } 
  }, 
  { 
    name: 'password', 
    value: '', 
    required: true, 
    options: { 
      type: 'password', 
      label: 'Your password' 
    }, 
    validate: { 
      maxLength: (v) => (v.trim().length < 6 ? 'Invalid' : ''), 
    }, 
  }, 
] 
 
const Auth = () => { 
  const dispatch = useDispatch(); 
  const history = useHistory(); 

  const { formArray, updateEvent, valid, disabled, submitEvent, setValueManually } = hookForm({ initialForm: form }) 
  const user = useSelector((s) => s.auth.user) 
  const registeredUser = useSelector((s) => s.auth.registeredUser); 

  useEffect(() => { 
    if (user) history.push('/'); 
  }, [user, history]); 
 
  useEffect(() => { 
    if (registeredUser) {
      setValueManually('email', registeredUser.email)
    } 
  }, [registeredUser, setValueManually]); 

  const submit = submitEvent((v) => dispatch(runAuth(v))); 
 
  return ( 
    <div className='auth'>
      <div> 
        <form className='form' onSubmit={submit}> 
          {formArray.map(el => ( 
            <div key={el.name}> 
              <Input 
                value={el.value}
                name={el.name} 
                label={el.options.label} 
                type={el.options.type}
                onChange={updateEvent} /> 
            </div> 
          ))} 
          <button type="submit" disabled={disabled || !valid}>Submit</button> 
          <a>If you are not registred?</a>
          <button onClick={() => history.push('/registred')}>Go to registred</button>
        </form> 
      </div>
    </div> 
  ) 
} 
 
export default Auth;
