import { Suspense, useEffect } from 'react'; 
import { useDispatch } from 'react-redux'; 
// import jwtDecode from 'jwt-decode'; 
import { Switch, Route, Redirect } from 'react-router-dom'; 
 
import { getMe } from '../../store/auth/actions'; 
import { getToken } from '../../utils/local-storage-actions'; 
import { ROUTES } from '../index'; 

const App = () => { 
  const dispatch = useDispatch(); 
 
  useEffect(() => { 
    const token = getToken(); 
    if (token) dispatch(getMe()) 
  }, [dispatch]) 
 
  return ( 
    <Switch> 
      <Suspense fallback={<div>Loading...</div>}> 
      {ROUTES.map((el) => ( 
        <Route 
          key={el.id} 
          exact={el.exact} 
          path={el.path} 
          render={(props) => { 
            const token = getToken(); 
            if (!token && el.private) return <Redirect to="/auth" /> 
            return <el.component {...props} /> 
          }} 
        /> 
      ))} 
      </Suspense> 
    </Switch> 
  ) 
} 
 
export default App;