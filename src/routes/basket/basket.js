import React, { useEffect, useState } from "react";

import Header from '../../component/header/header'

import './basket.scss'

const img_url = 'https://image.tmdb.org/t/p/w1280';

const Favorites = (key) => { 
  const [ deleteLocal, setDeleteLocal ] = useState(null)
  const [ renderBasket, setRenderBasket ] = useState(true)

  useEffect(() => {
    if(deleteLocal !== null){
      let movies = JSON.parse(localStorage.getItem('movies'));
      movies.splice(deleteLocal, 1);
      localStorage.setItem('movies', JSON.stringify(movies));
      // document.location.reload();
      setRenderBasket((prevState) => !prevState)
    }
  }, [deleteLocal])

  console.log(renderBasket)

  const currentMovies = JSON.parse(localStorage.getItem("movies") || "[]");
  
  const buildMyList = () => {
    return currentMovies.filter(item => item.img_path).map((item, i) => { 
      return <div key={i} className='favorites_item_card'>
        <img src={img_url + item.img_path} alt={item.title}/>
        <div className='favorites_item_card_info'>
          <div className='favorites_item_card_info_text'>
            <h2>{ item.title }</h2>
            <p>{ item.overview}</p>
          </div>
          <button onClick={() => {setDeleteLocal(i)}}> Remove from favorite</button>
        </div>
      </div>
    });
  }
  
  return ( 
   <>
   <Header />
      <div className='favorites'> 
      <h2>My List</h2>
        <div className='favorites_item'>
          {currentMovies.length > 0 ? <div className='favorites_item_total'> { buildMyList() }</div> : <p>There are no items in your list at this moment</p>}
        </div>
      </div>
   </> 
  ) 
} 
 
export default Favorites;
