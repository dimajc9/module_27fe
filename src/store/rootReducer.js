import { combineReducers } from "redux";
import { authReducer } from './auth/reducer'

const reducers = combineReducers({
    auth: authReducer,
})

export const rootReduser = (state, action) => {
    return reducers(state, action)
}
