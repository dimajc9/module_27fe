export const RUN_AUTH = 'RUN_AUTH'; 
export const RUN_AUTH_SUCCESS = 'RUN_AUTH_SUCCESS'; 
export const RUN_AUTH_FAIL = 'RUN_AUTH_FAIL'; 
 
export const GET_ME = 'GET_ME'; 
export const GET_ME_SUCCESS = 'GET_ME_SUCCESS'; 
export const GET_ME_FAIL = 'GET_ME_FAIL';

export const RUN_REGISTER = 'RUN_REGISTER'; 
export const RUN_REGISTER_SUCCESS = 'RUN_REGISTER_SUCCESS'; 
export const RUN_REGISTER_FAIL = 'RUN_REGISTER_FAIL';
