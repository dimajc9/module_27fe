import * as types from './types'; 
 
const initialState = { 
  user: null, 
  registeredUser: null,
  
} 
 
export const authReducer = (state = initialState, action) => { 
  switch(action.type) { 
    case types.RUN_AUTH_SUCCESS: { 
      return { ...state, user: action.payload } 
    } 
    case types.RUN_AUTH:  
    case types.RUN_AUTH_FAIL: 
    case types.GET_ME:  
    case types.GET_ME_FAIL: { 
      return state; 
    } 
 
    case types.GET_ME_SUCCESS:  { 
      return { ...state, user: action.payload } 
    }
    
    case types.RUN_REGISTER_SUCCESS: { 
      return { ...state, registeredUser: action.payload}
    }
 
    default: { 
      return state; 
    } 
  } 
}
