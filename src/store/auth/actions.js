import * as types from './types'; 
 
export const runAuth = (payload) => ({ 
  type: types.RUN_AUTH, 
  payload 
}) 
 
export const runAuthSubmit = (payload) => ({ 
  type: types.RUN_AUTH_SUCCESS, 
  payload 
}) 
 
export const runAuthFail = (payload) => ({ 
  type: types.RUN_AUTH_FAIL, 
  payload 
}) 
 
export const getMe = (payload) => ({ 
  type: types.GET_ME, 
  payload 
}) 
 
export const getMeSuccess = (payload) => ({ 
  type: types.GET_ME_SUCCESS, 
  payload 
}) 
 
export const getMeFail = (payload) => ({ 
  type: types.GET_ME_FAIL, 
  payload 
})

export const runRegister = (payload) => ({ 
  type: types.RUN_REGISTER, 
  payload 
}) 
 
export const runRegisterSuccess = (payload) => ({ 
  type: types.RUN_REGISTER_SUCCESS, 
  payload 
}) 
 
export const runRegisterFail = (payload) => ({ 
  type: types.RUN_REGISTER_FAIL, 
  payload 
})
