import { takeLatest, call, put } from 'redux-saga/effects'; 
 
import * as types from './types'; 
import * as actions from './actions' 
import { AxiosInstance } from '../../utils/axios' 

import { getToken, setToLocalStorage } from '../../utils/local-storage-actions'; 

const authRequest = (cred) => fetch('http://localhost:8000/auth/login', { 
  method: 'POST', 
  headers: { 
    Authorization: `Basic ${cred}`
  } 
}).then(r => r.json()) 
 
const meRequest = (token) => AxiosInstance({ 
  url: 'users/me', 
}) 
 
const register = (cred) => AxiosInstance({ 
  url: 'auth/register', 
  method: 'POST', 
  data: cred, 
  headers: { Authorization: `Bearer super-secret-master-key` }, 
}) 
 
function* runAuth({ payload }) { 
  const credentials = yield window.btoa(`${payload.email}:${payload.password}`) 
 
  try { 
    const result = yield call(authRequest, credentials); 
 
    yield setToLocalStorage(result.token) 
 
    yield put(actions.runAuthSubmit(result.user)) 
  } catch (error) { 
    yield put(actions.runAuthFail(error)) 
  } 
} 

function* runRegister({ payload }) { 
  try { 
    const result = yield call(register, payload); 
    console.log('result', result) 
    yield put(actions.runRegisterSuccess(result.data)) 
  } catch (error) { 
    yield put(actions.runRegisterFail(error)) 
  } 
} 
 
function* getMe({ payload }) { 
  try { 
    const token = getToken(); 
    const result = yield call(meRequest, token); 
 
    yield put(actions.getMeSuccess(result.data)) 
  } catch (error) { 
    yield put(actions.getMeFail(error)) 
  } 
} 
 
export default function* auth() { 
  yield takeLatest(types.RUN_AUTH, runAuth); 
  yield takeLatest(types.GET_ME, getMe); 
  yield takeLatest(types.RUN_REGISTER, runRegister); 
}
