import { applyMiddleware, createStore } from 'redux'
import logger from 'redux-logger'
import createSagaMiddleware from 'redux-saga'

import { rootReduser } from './rootReducer'
import rootSaga from './saga'

const sagaMiddleware = createSagaMiddleware()

const middlewares = [sagaMiddleware]

if(process.env.MODE_ENV === 'development'){
    middlewares.push(logger)
}

const store = createStore(rootReduser, applyMiddleware(...middlewares, logger))

export default store 
 
sagaMiddleware.run(rootSaga)
