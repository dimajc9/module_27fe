import React from 'react'

const Input = ({ 
  id = Math.random().toString(), 
  label, 
  touched, 
  error, 
  value, 
  onChange, 
  name, 
  type 
}) => { 
  return ( 
    <div > 
      {label && <label htmlFor={id} >{label}</label>} 
      <input type={type} id={id} name={name} value={value} onChange={onChange}/> 
      {touched && error && <span >{error}</span>} 
    </div> 
  ) 
} 
 
export default Input;
