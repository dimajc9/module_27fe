import React, { useState } from 'react'; 

import { useHistory } from 'react-router-dom'; 
import { logOut } from '../../utils/local-storage-actions'; 
import { useSelector } from 'react-redux';

import './header.scss'

const Header = () => {
    const [ modal, setModal ] = useState(false)

    const history = useHistory();
    const user = useSelector((s) => s.auth.user?.email);

    return(
        <header className='header'>
            { modal && <div className='modal_hamburger'>
                <div className='imgEsc'>
                    <img onClick={() => {setModal(false)}} src="https://svgsilh.com/svg_v2/146131.svg" alt="alt" />
                </div>
                <div className='modal_hamburger_center'>
                    <button className='logOut' onClick={logOut}>LogOut</button>
                    <button onClick={() => history.push('/favorite')}>Favorites</button>
                    <button onClick={() => history.push('/')}>Films</button>
                </div>
            </div> }
            { !modal && <div className='header_center_block'>
                <div className='singlUp'>
                    <p>{user}</p>
                    <button className='logOut' onClick={logOut}>LogOut</button>
                </div>
                <div className='baskets'>
                    <button onClick={() => history.push('/favorite')}>Favorites</button>
                    <button onClick={() => history.push('/')}>Films</button>
                </div>
                <div className='hamburger'>
                    <img onClick={() => {setModal(true)}} src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Hamburger_icon.svg/1024px-Hamburger_icon.svg.png" alt="alt" />
                </div>
            </div> }
        </header>
    )
}

export default Header
